package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.ExecutedTransactionTree;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.abicodegen.TokenContract;
import com.secata.stream.SafeDataOutputStream;
import java.io.IOException;
import java.math.BigInteger;

/**
 * This example demonstrates how to use the AbiCodegen maven plugin together with the Blockchain
 * Client to send a transaction both to deploy a contract. The example uses the TestNet and a
 * provided private key, the associated account to the key needs to have enough gas to send the
 * transaction.
 */
public final class AbiCodegenContractDeploy {

  /** Private key to use to signed and send transactions. */
  private static final String PRIVATE_KEY = "aa";

  /** The amount of gas to pass to the deployment call. */
  private static final int DEPLOYMENT_GAS = 2500000;

  /** The address of the public deploy contract. */
  public static final BlockchainAddress PUB_DEPLOY_ADDRESS =
      BlockchainAddress.fromString("0197a0e238e924025bad144aa0c4913e46308f9a4d");

  /** Supply of the token. */
  private static final long MY_SUPPLY = 100_000_000L;

  /** File name for the WASM of the token contract. */
  private static final String WASM_PATH = "/token_contract.wasm";

  /** File name for the ABI of the token contract. */
  private static final String ABI_PATH = "/token_contract.abi";

  /**
   * MainMethod for deploying a contract on PBC.
   *
   * @param args main arguments
   * @throws IOException in case of missing files.
   */
  public static void main(String[] args) throws IOException {

    final BlockchainClient blockchainClient =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);

    // KeyPair create with the secret key provided
    final KeyPair keyPair = new KeyPair(new BigInteger(PRIVATE_KEY, 16));

    // Create client which can be used for sending transactions to the blockchain
    final BlockchainTransactionClient txClient =
        BlockchainTransactionClient.create(
            blockchainClient, new SenderAuthenticationKeyPair(keyPair));

    // Deployment arguments
    byte[] initBytes =
        TokenContract.initialize("MyToken", "MT", (byte) 8, BigInteger.valueOf(MY_SUPPLY));
    byte[] wasmBytes = loadBytes(WASM_PATH);
    byte[] abiBytes = loadBytes(ABI_PATH);

    // Create the transaction
    final Transaction transaction = buildDeploymentTransaction(wasmBytes, abiBytes, initBytes);

    // Send the transaction
    final SentTransaction sentTransaction = txClient.signAndSend(transaction, DEPLOYMENT_GAS);

    // Wait for the transaction to be included in a block and all spawned events are handled.
    txClient.waitForSpawnedEvents(sentTransaction);

    // Wait to make sure that the transaction has been included in a block before exiting
    ExecutedTransactionTree executedTransactionTree =
        txClient.waitForSpawnedEvents(sentTransaction);

    // Identifier, which is used to determine the deployment address.
    Hash identifier = Hash.fromString(executedTransactionTree.executedTransaction().identifier());

    // Determine the Blockchain address from the identifier and the contract type.
    BlockchainAddress deployedAt =
        BlockchainAddress.fromHash(BlockchainAddress.Type.CONTRACT_PUBLIC, identifier);
    System.out.println(deployedAt);
  }

  /**
   * Create the deployment transaction to deploy the token contract.
   *
   * @param wasm the contract wasm to deploy.
   * @param abi the contract abi.
   * @param initRpc the initial arguments as serialized bytes
   * @return the contract deployment action.
   */
  static Transaction buildDeploymentTransaction(byte[] wasm, byte[] abi, byte[] initRpc) {

    // Action on the public-deploy contract
    byte[] deployShortname = new byte[] {1};
    // Serialize the arguments to the action of the public-deploy contract
    byte[] transactionRpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.write(deployShortname);
              s.writeDynamicBytes(wasm);
              s.writeDynamicBytes(abi);
              s.writeDynamicBytes(initRpc);
            });
    // Build the transaction
    return Transaction.create(PUB_DEPLOY_ADDRESS, transactionRpc);
  }

  /**
   * Function that reads a file from filepath and filename as stream and return it as byte[].
   *
   * @param filename the filepath ending with filename ot be read as stream
   * @return the file as a byte[]
   * @throws IOException IOException
   */
  private static byte[] loadBytes(String filename) throws IOException {
    return ContractDeploy.class.getResourceAsStream(filename).readAllBytes();
  }
}
