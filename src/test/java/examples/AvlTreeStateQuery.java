package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.abicodegen.TokenV2;
import com.partisiablockchain.language.codegenlib.BlockchainStateClientImpl;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * This example demonstrates how to get the state of an MPC20-v2 contract. Specifically how to query
 * the reader node for specific entries of an AvlTreeMap.
 */
public final class AvlTreeStateQuery {
  public static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromString("02e1e09358e543e8b1cf97d5e19d5b287983cee8f6");

  public static final String TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

  /**
   * Main.
   *
   * @param args args
   */
  public static void main(String[] args) {
    final BlockchainClient blockchainClient = BlockchainClient.create(TESTNET_URL, 3);

    // Using stateOutput=binary we only get the state bytes.
    ContractState contractState =
        blockchainClient.getContractState(CONTRACT_ADDRESS, BlockchainClient.StateOutput.BINARY);
    byte[] stateBytes =
        ExceptionConverter.call(() -> contractState.serializedContract().binaryValue());

    // Codegen can deserialize normal state.
    TokenV2.TokenState state = TokenV2.deserializeState(stateBytes);
    // Because balances are stored in an AVL tree, can only get tree id
    System.out.printf("Balance map id: %s%n", state.balances().treeId());
    // All other fields are available
    System.out.printf("name: %s%n", state.name());

    // To query an AVL tree map you need to create a token contract with a client and an address.
    TokenV2 tokenContract =
        new TokenV2(BlockchainStateClientImpl.create(TESTNET_URL), CONTRACT_ADDRESS);

    // You can then fetch the state using getState
    state = tokenContract.getState();

    // You can then query the blockchain for avl values directly using the codegen
    BlockchainAddress queryAddress =
        BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5");
    BigInteger value = state.balances().get(queryAddress);
    System.out.printf("value: %d%n", value);

    BlockchainAddress owner =
        BlockchainAddress.fromString("00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed");
    BlockchainAddress spender =
        BlockchainAddress.fromString("009a693442a90730e353490199aa71e87b75ac2e43");

    BigInteger allowedValue = state.allowed().get(new TokenV2.AllowedAddress(owner, spender));
    System.out.printf("allowed value: %d%n", allowedValue);

    // It is also possible to query multiple avl entries at once using the getNextN
    // Using this it is possible to iterate through all entries
    System.out.println("\nFirst 100 balances:");
    BlockchainAddress lastKey = null;
    for (int i = 0; i < 10; i++) {
      List<Map.Entry<BlockchainAddress, BigInteger>> entries =
          state.balances().getNextN(lastKey, 10);
      lastKey = entries.get(entries.size() - 1).getKey();
      for (Map.Entry<BlockchainAddress, BigInteger> entry : entries) {
        System.out.printf("%s: %d%n", entry.getKey().writeAsString(), entry.getValue());
      }
    }
  }
}
