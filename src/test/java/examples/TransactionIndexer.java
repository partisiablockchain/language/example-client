package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ExecutedTransaction;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/** This example demonstrates how to read the latest ten transactions. */
public final class TransactionIndexer {

  /**
   * Show the latest 10 transactions on the blockchain.
   *
   * @param args commandline arguments
   */
  public static void main(String[] args) {

    final BlockchainClient client =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);

    BlockIterator blocks = new BlockIterator(client);

    // Print the 10 latest transactions.
    int numTx = 0;
    while (numTx < 10) {

      BlockIterator.BlockData blockData = blocks.next();

      // Print the transactions of the block
      ShardId shardId = blockData.shardId();
      BlockState blockState = blockData.blockState();
      if (!blockState.transactions().isEmpty()) {
        Instant blockInstant = Instant.ofEpochMilli(blockState.productionTime());
        System.out.printf(
            "Block %s %s (%s)%n", blockInstant.toString(), blockState.identifier(), shardId.name());
        for (String transactionHash : blockState.transactions()) {
          numTx++;
          ExecutedTransaction transaction =
              client.getTransaction(shardId, Hash.fromString(transactionHash));
          System.out.printf("  Transaction: %s %n", transaction.identifier());
        }
      }
    }
  }

  /** Iterates blocks across shards in production time order. */
  public static final class BlockIterator implements Iterator<BlockIterator.BlockData> {

    /** Data for a block on a shard. */
    public record BlockData(ShardId shardId, BlockState blockState) {}

    /** The blockchain client used to fetch more blocks. */
    private final BlockchainClient client;

    /**
     * Holds the newest block for each shard that happened right after the cursor of the iterator.
     */
    private final List<BlockData> nextShardBlocks;

    /**
     * Create a Block Iterator with the given blockchain client. Loads the latest block for each
     * shard.
     *
     * @param client the blockchain client used for communicating with the reader nodes.
     */
    public BlockIterator(BlockchainClient client) {
      this.client = client;
      // Find the latest block for each shard.
      List<BlockData> shardBlocks = new ArrayList<>();
      for (ShardId shardId : client.getShardManager().getShards()) {
        BlockState blockState = client.getLatestBlock(shardId);
        shardBlocks.add(new BlockData(shardId, blockState));
      }
      this.nextShardBlocks = shardBlocks;
    }

    /**
     * Returns the next element in iteration without advancing the underlying iterator.
     *
     * @return the next element in iteration. Null if there are no more elements.
     */
    public BlockData peek() {
      return nextShardBlocks.stream()
          .max(Comparator.comparingLong(o -> o.blockState.productionTime()))
          .orElse(null);
    }

    @Override
    public boolean hasNext() {
      return peek() != null;
    }

    @Override
    public BlockData next() {
      BlockData next = peek();
      if (next != null) {
        nextShardBlocks.remove(next);
        ShardId shardId = next.shardId;
        BlockState nextShardBlock =
            client.getBlockByHash(shardId, Hash.fromString(next.blockState.parentBlock()));
        if (nextShardBlock != null) {
          nextShardBlocks.add(new BlockData(shardId, nextShardBlock));
        }
      }
      return next;
    }
  }
}
