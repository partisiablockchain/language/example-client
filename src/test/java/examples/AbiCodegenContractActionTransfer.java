package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.language.abicodegen.TokenContract;
import java.math.BigInteger;

/**
 * The example demonstrates how to use the generated code made with the maven plugin
 * "abi-generation-maven-plugin", to send a transfer action to a deployed token contract. The
 * contract is deployed on the Testnet. The rpc for the transfer call is made with a generated
 * class.
 */
public final class AbiCodegenContractActionTransfer {

  /** Address of the Token Contract we are sending a transaction to. */
  private static final BlockchainAddress TOKEN_ADDRESS =
      BlockchainAddress.fromString("02094c90ea2d4a5545de3000046a3cc27ffd7a4f41");

  /** Gas to use for the transaction. */
  private static final long GAS = 10000L;

  /** Address of the receiving party. */
  private static final BlockchainAddress RECEIVER =
      BlockchainAddress.fromString("00db4eb445882bdb5c40c5959d1260d9383035b4e5");

  /** Private key to use for signing. */
  private static final String PRIVATE_KEY = "aa";

  /**
   * MainMethod for sending a transfer to a token contract on PBC.
   *
   * @param args main arguments
   */
  public static void main(String[] args) {

    // Create a client that can talk to the blockchain. In this case the testnet.
    final BlockchainClient blockchainClient =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);

    // KeyPair create with the secret key provided, this should also be the one that deployed the
    // token contract or have already been transferred some tokens from the owner of the contract.
    final KeyPair keyPair = new KeyPair(new BigInteger(PRIVATE_KEY, 16));

    // Create client which can be used for sending transactions to the blockchain
    final BlockchainTransactionClient txClient =
        BlockchainTransactionClient.create(
            blockchainClient, new SenderAuthenticationKeyPair(keyPair));

    // Create the RPC bytes for a call to transfer
    byte[] rpc = TokenContract.transfer(RECEIVER, BigInteger.valueOf(10));

    // Create the transaction
    final Transaction transaction = Transaction.create(TOKEN_ADDRESS, rpc);

    // Sign the transaction and send it onto the blockchain
    final SentTransaction sentTransaction = txClient.signAndSend(transaction, GAS);

    // Wait to make sure that the transaction has been included in a block before exiting
    txClient.waitForSpawnedEvents(sentTransaction);
  }
}
