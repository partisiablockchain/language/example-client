package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HexFormat;

/**
 * This example demonstrates how to create a transaction that deploys a contract to the Partisia
 * Blockchain. The contract deployed is our voting-example-contract which creates a vote based on a
 * proposal ID, it takes eligible voter addresses and vote deadline in millis. The contract ends
 * when the millis deadline has been reached and publishes the result on PBC
 */
public final class ContractDeploy {
  /** The address of the public deploy contract. */
  public static final BlockchainAddress PUB_DEPLOY_ADDRESS =
      BlockchainAddress.fromString("0197a0e238e924025bad144aa0c4913e46308f9a4d");

  /**
   * The private key of the account interacting with the blockchain. Make sure that this account has
   * enough gas on the testnet or the transaction will fail.
   */
  private static final String PRIVATE_KEY = "00112233445566778899aabbccddeeff";

  /**
   * MainMethod for deploying a contract on PBC.
   *
   * @param args main arguments
   * @throws IOException in case of missing files.
   */
  public static void main(String[] args) throws IOException {

    // Create a client that can talk to the blockchain. In this case the testnet.
    final BlockchainClient blockchainClient =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);

    // Create a key pair from the private key that can be used for sending transactions
    final KeyPair keyPair = new KeyPair(new BigInteger(PRIVATE_KEY, 16));

    // Create client which can be used for sending transactions to the blockchain
    final BlockchainTransactionClient txClient =
        BlockchainTransactionClient.create(
            blockchainClient, new SenderAuthenticationKeyPair(keyPair));

    // Create bytearrays for the abi and wasm file of the compiled contract to be deployed.
    byte[] abiBytes = loadBytes("/voting.abi");
    byte[] wasmBytes = loadBytes("/voting.wasm");

    // Create the initializing rpc for the init step of the rust contract
    String initRpc =
        "0000000000000001" // proposal ID
            + "00000002" // number of eligible voters
            + "00e7c010cea8cc3e1891ba19e530e596b9f2b4d4fe" // eligible voter address
            + "00edf544fe0d1bd742da3b5ff2fde115b77962ba87" // eligible voter address
            + "00000000000927c0" // vote deadline millis
        ;

    // Send the transaction with wasm, abi and parameters for the contract.
    final Transaction transaction = buildDeployTransaction(wasmBytes, abiBytes, initRpc);

    // The amount of gas sent with the transaction to handle the cost of running it
    final int gas = 2272015;

    // Sign the transaction and send it onto the blockchain
    final SentTransaction sentTransaction = txClient.signAndSend(transaction, gas);
    // Print the transaction pointer making it is easy to find it on the chain
    System.out.println(sentTransaction.transactionPointer());
    // Wait to make sure that the transaction has been included in a block before exiting
    txClient.waitForSpawnedEvents(sentTransaction);
  }

  //

  /**
   * Function that reads a file from filepath and filename as stream and return it as byte[].
   *
   * @param filename the filepath ending with filename ot be read as stream
   * @return the file as a byte[]
   * @throws IOException IOException
   */
  private static byte[] loadBytes(String filename) throws IOException {
    return ContractDeploy.class.getResourceAsStream(filename).readAllBytes();
  }

  /**
   * Build the transaction to deploy a new contract on the blockchain. The transaction is sent to an
   * existing public-deploy contract on the blockchain, which then deploys the new contract.
   *
   * @param wasm the bytecode of the contract to deploy
   * @param abi the abi of the contract
   * @param initRpc The values needed for deployTransaction contract
   * @return the created transaction
   */
  static Transaction buildDeployTransaction(byte[] wasm, byte[] abi, String initRpc) {

    // Action on the public-deploy contract
    byte[] deployShortname = new byte[] {1};
    // Needed bytes in front of the bytecode for the blockchain
    byte[] initHeaderBytes = HexFormat.ofDelimiter(":").parseHex("FF:FF:FF:FF:0F");
    // Convert to hex the initialization parameters
    byte[] initializationRpc = HexFormat.of().parseHex(initRpc);
    // Serialize the arguments to the action of the public-deploy contract
    byte[] transactionRpc =
        SafeDataOutputStream.serialize(
            s -> {
              s.write(deployShortname);
              s.writeDynamicBytes(wasm);
              s.writeDynamicBytes(abi);
              s.writeDynamicBytes(concatBytes(initHeaderBytes, initializationRpc));
            });
    // Build the transaction
    return Transaction.create(PUB_DEPLOY_ADDRESS, transactionRpc);
  }

  /**
   * Concatenation of two or more byte arrays.
   *
   * @param bytes the byte[] that needs to be concatenated separated by a comma
   * @return a new bytearray in the order of the parameters.
   */
  private static byte[] concatBytes(byte[]... bytes) {
    var out = new ByteArrayOutputStream();
    for (byte[] array : bytes) {
      out.writeBytes(array);
    }
    return out.toByteArray();
  }
}
