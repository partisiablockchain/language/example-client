package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SentTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.secata.stream.SafeDataOutputStream;
import java.math.BigInteger;
import org.bouncycastle.util.encoders.Hex;

/**
 * This example demonstrates how to create a transaction and send it to the Partisia Blockchain. The
 * transaction mints 1000 TEST bring your own coins on the testnet by calling the BYOC faucet
 * contract. The 1000 TEST coins converts to 1 million gas, which can be used for running
 * transactions on the testnet.
 */
final class ByocMint {

  /** The address of the BYOC TEST coin faucet contract. */
  public static final BlockchainAddress BYOC_FAUCET_ADDRESS =
      BlockchainAddress.fromString("01860476afca938871ff2c49bf5490235445942e3e");

  /**
   * The private key of the account interacting with the blockchain. Make sure that this account has
   * enough gas on the testnet or the transaction will fail.
   */
  private static final String PRIVATE_KEY = "00112233445566778899aabbccddeeff";

  public static void main(String[] args) {
    // Address of the account that should receive the minted TEST coins
    String recipient = "00e7c010cea8cc3e1891ba19e530e596b9f2b4d4fe";

    // Create a client that can talk to the blockchain. In this case the testnet.
    final BlockchainClient blockchainClient =
        BlockchainClient.create("https://node1.testnet.partisiablockchain.com", 3);

    // Create a key pair from the private key that can be used for sending transactions
    final KeyPair keyPair = new KeyPair(new BigInteger(PRIVATE_KEY, 16));

    // Create client which can be used for sending transactions to the blockchain
    final BlockchainTransactionClient txClient =
        BlockchainTransactionClient.create(
            blockchainClient, new SenderAuthenticationKeyPair(keyPair));

    // Create a transaction where the faucet is the receiving contract and a payload containing the
    // address of the account that will receive the minted TEST coins.
    final Transaction transaction =
        Transaction.create(
            BYOC_FAUCET_ADDRESS,
            SafeDataOutputStream.serialize(s -> s.write(Hex.decode(recipient))));

    // The amount of gas sent with the transaction to handle the cost of running it
    final int gas = 5910;

    // Sign the transaction and send it onto the blockchain
    final SentTransaction sentTransaction = txClient.signAndSend(transaction, gas);
    // Print the transaction pointer making it is easy to find it on the chain
    System.out.println(sentTransaction.transactionPointer());
    // Wait to make sure that the transaction has been included in a block before exiting
    txClient.waitForSpawnedEvents(sentTransaction);
  }
}
