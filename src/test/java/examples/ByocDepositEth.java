package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Bytes21;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.RawTransactionManager;

/**
 * Demonstrates how to bridge ETH from The Ethereum Blockchain to Partisia Blockchain as
 * Bring-Your-Own-Coin (BYOC).
 *
 * <p>This is done by sending a transaction with the ETH on the Ethereum blockchain to the Partisia
 * BYOC deposit contract. This code can easily be modified to for bridgning BYOC to Partisia from
 * all supported EVM chains.
 *
 * <p>To run the code you must create the following files:
 *
 * <ul>
 *   <li><code>~/.pbc/eth.endpoint</code> Containing the URL of the ETH RPC Endpoint to use for
 *       sending the transaction.
 *   <li><code>~/.pbc/eth.pk</code> Containing the private key to use for signing the ETH
 *       transaction in hex format.
 * </ul>
 *
 * <p>When running you must pass the following commandline arguments
 *
 * <ul>
 *   <li>The Partisia Blockchain address of the recipient.
 *   <li>The amount of WEIs to bridge. The minimum is 0.01 ETH = 10000000000000000 WEI.
 * </ul>
 */
public final class ByocDepositEth {

  /** URL of the JSON-RPC ETH endpoint to use for sending the transaction. Loaded from file. */
  public static final String ETH_ENDPOINT_URL = loadFromFile("~/.pbc/eth.endpoint");

  /** Private key to use for signing the ETH transaction. Loaded from file. */
  public static final String ETH_PRIVATE_KEY = loadFromFile("~/.pbc/eth.pk");

  /** The BYOC deposit contract on Ethereum. */
  public static final String ETH_BYOC_CONTRACT = "0x4818370f9d55fb34De93E200076533696c4531f3";

  /** The BYOC deposit action on the ETH contract. */
  public static final String ETH_BYOC_CONTRACT_FUNCTION = "deposit";

  /** The ETH gas price to use. */
  public static final BigInteger ETH_GAS_PRICE = BigInteger.valueOf(55000000000L);

  /** The ETH gas limit for the transaction. */
  public static final BigInteger ETH_GAS_LIMIT = BigInteger.valueOf(50491L);

  /**
   * Bridge ETH to Partisia Blockchain as BYOC.
   *
   * @param args Command-line arguments. (Not used)
   */
  public static void main(String[] args) {
    if (args.length != 2) {
      throw new RuntimeException("USAGE: ByocDepositEth <pbc-address> <amount-wei>");
    }
    // The address of the recipient account on PBC.
    BlockchainAddress recipientPbcAddress = BlockchainAddress.fromString(args[0]);
    // The amount of WEI to bridge to PBC
    BigInteger transferAmountWei = new BigInteger(args[1]);

    try {
      // Connect to the ETH JSON-RPC endpoint
      Web3j web3j = Web3j.build(new HttpService(ETH_ENDPOINT_URL));
      // Create credentials for signing the transaction
      Credentials credentials = Credentials.create(ETH_PRIVATE_KEY);
      // Look up the transaction nonce to use
      BigInteger nonce = getNonce(web3j, credentials.getAddress());
      // Create a function call to the deposit contract to bridge the ETH
      Bytes21 pbcRecipient =
          new Bytes21(SafeDataOutputStream.serialize(recipientPbcAddress::write));
      Function function =
          new Function(ETH_BYOC_CONTRACT_FUNCTION, List.of(pbcRecipient), List.of());
      String inputData = FunctionEncoder.encode(function);
      RawTransaction rawTransaction =
          RawTransaction.createTransaction(
              nonce, ETH_GAS_PRICE, ETH_GAS_LIMIT, ETH_BYOC_CONTRACT, transferAmountWei, inputData);
      // Sign and send the transaction
      RawTransactionManager txManager = new RawTransactionManager(web3j, credentials);
      EthSendTransaction sentTransaction = txManager.signAndSend(rawTransaction);
      if (sentTransaction.hasError()) {
        throw new RuntimeException(sentTransaction.getError().getMessage());
      }
      // Print link to the transaction on the ETH block explorer
      System.out.println("Successfully sent transaction " + sentTransaction.getTransactionHash());
    } catch (Exception e) {
      System.err.println("Error bridging BYOC\n" + e);
    }
    // Exit to stop Web3j background processes
    System.exit(0);
  }

  /**
   * Look up the transaction nonce to use for sending a transaction.
   *
   * @param web3j The ETH JSON-RPC factory
   * @param fromAddress The address to find nonce for.
   * @return The transaction nonce to use.
   */
  private static BigInteger getNonce(Web3j web3j, String fromAddress) throws IOException {
    EthGetTransactionCount ethGetTransactionCount =
        web3j.ethGetTransactionCount(fromAddress, DefaultBlockParameterName.PENDING).send();
    return ethGetTransactionCount.getTransactionCount();
  }

  /**
   * Load a string from a file.
   *
   * @param file The filename to load from. Any '~' character in the file is replaced with the users
   *     home folder.
   * @return The String loaded from the file.
   * @throws RuntimeException If the file does not exist.
   */
  private static String loadFromFile(String file) {
    String absFile = file.replace("~", System.getProperty("user.home"));
    String str =
        ExceptionConverter.call(
            () -> Files.readString(Path.of(absFile)), "Cannot load file " + file);
    return str.replaceAll("[ \n\r]", "");
  }
}
