package examples;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainClient;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.language.abimodel.model.ContractAbi;
import com.partisiablockchain.language.abimodel.parser.AbiParser;
import com.partisiablockchain.language.contractstandard.ContractStandards;
import com.partisiablockchain.language.contractstandard.Mpc20ContractState;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This example show how to get the standard state of a MPC20 contract at two different block times,
 * comparing their state and finding the modified entries of the balance map.
 */
public final class Mpc20StateDiff {

  /** Address of a MPC20 contract on Mainnet. */
  public static final BlockchainAddress CONTRACT_ADDRESS =
      BlockchainAddress.fromString("020248889171091d122569a6801bdc739ceab215bf");

  /**
   * Main.
   *
   * @param args args
   */
  public static void main(String[] args) {
    final BlockchainClient blockchainClient =
        BlockchainClient.create("https://reader.partisiablockchain.com", 3);

    int blockTime1 = 2855689;
    int blockTime2 = 2855690;
    Mpc20ContractState state1 = getMpc20State(blockchainClient, CONTRACT_ADDRESS, blockTime1);
    Mpc20ContractState state2 = getMpc20State(blockchainClient, CONTRACT_ADDRESS, blockTime2);

    Map<BlockchainAddress, BigInteger> difference =
        getDifference(state1.balances(), state2.balances());

    System.out.printf(
        "Values that have changed between block time %s and %s%n", blockTime1, blockTime2);
    for (Map.Entry<BlockchainAddress, BigInteger> entry : difference.entrySet()) {
      System.out.printf("%s: %s%n", entry.getKey().writeAsString(), entry.getValue());
    }
  }

  /**
   * Get the standard state of a MPC20 contract.
   *
   * @param client blockchain client
   * @param address address of the contract
   * @param blockTime the block time for which to get the state at
   * @return the standard state of the contract
   */
  private static Mpc20ContractState getMpc20State(
      BlockchainClient client, BlockchainAddress address, long blockTime) {
    ContractState contractState =
        client.getContractState(address, BlockchainClient.StateOutput.BINARY, blockTime);
    byte[] stateBytes =
        ExceptionConverter.call(() -> contractState.serializedContract().binaryValue());
    ContractAbi abi = new AbiParser(contractState.abi()).parseAbi().contract();
    return ContractStandards.MPC20.getStandardState(stateBytes, abi);
  }

  /**
   * Get all the modified entries between two maps.
   *
   * @param balances1 map one
   * @param balances2 map two
   * @return the difference between the maps
   */
  private static Map<BlockchainAddress, BigInteger> getDifference(
      Map<BlockchainAddress, BigInteger> balances1, Map<BlockchainAddress, BigInteger> balances2) {
    Map<BlockchainAddress, BigInteger> result = new HashMap<>();
    Set<BlockchainAddress> keys1 = balances1.keySet();
    for (BlockchainAddress address : keys1) {
      if (!balances1.get(address).equals(balances2.get(address))) {
        result.put(address, balances2.get(address));
      }
    }
    Set<BlockchainAddress> keys2 = balances2.keySet();
    keys2.removeAll(keys1);
    for (BlockchainAddress address : keys2) {
      result.put(address, balances2.get(address));
    }
    return result;
  }
}
